/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.korn.Databaseproject.model.Customer;
import com.korn.Databaseproject.service.CustomerService;

/**
 *
 * @author Good
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getBytel("0888868888"));
        Customer cus1 = new Customer("Leo", "0971608591");
        cs.addNew(cus1);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getBytel("0971608591");
        delCus.setTel("0971608598");
        cs.update(delCus);
        for(Customer customer: cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete(delCus);        
        for(Customer customer: cs.getCustomers()){
            System.out.println(customer);
        }
    }
    
}
    
  